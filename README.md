# sQTL mapping for Grgic et al., 2021: “CYP11B1 variants influence skeletal maturation via alternative splicing” 

This repository contains various setting files and command line arguments used to generate the sQTL analysis from Grgic et al., 2021. 

This requires access to protected human data at dbGaP from GTEx v8 [phs000424.v8.p2](https://www.ncbi.nlm.nih.gov/projects/gap/cgi-bin/study.cgi?study_id=phs000424.v8.p2). This includes RNA-seq data from 208 donors with adrenal gland tissue (donor IDs listed in this repository at `donor_list.txt`) and the VCF file containing phased genotype information based on whole genome sequencing processed by (GTEx Consortium, 2020) (file `GTEx_Analysis_2017-06-05_v8_WholeGenomeSeq_838Indiv_Analysis_Freeze.SHAPEIT2_phased.vcf.gz` downloaded from dbGaP). Additional requirements include [MAJIQ](https://majiq.biociphers.org/app_download/) version 2.1 (Vaquero-Garcia et al., 2016) to quantify PSI in the 208 donors and [MAJIQtl](https://bitbucket.org/biociphers/majiq_sqtl/src/master/) (Nurnberg et al., 2020) to call sQTLs. 

## RNA-seq processing 
RNA-seq data corresponding to the 208 adrenal samples (`sample_list`) from donors (in `donor_list.txt`) were downloaded from dbGaP processed as described in detail previously to generate aligned and indexed bam files using `STAR 2.5.3a` with the option `--alignSJoverhangMin 8` (Aicher et al., 2020, Grgic et al., 2021). 

## MAJIQ 
### MAJIQ build
All GTEx RNA-seq samples were built incrementally, as in (Aicher et al., 2020), with the following command:

`majiq build --incremental --nproc 2 --conf build.ini ensembl.94.Homo_sapiens.GRCh38/ensembl.Homo_sapiens.GRCh38.94.gff3 --junc-files-only --min-experiments 0.5 --min-intronic-cov 0.01 --min-denovo 5 --minreads 3 --minpos 2 --m 30 --irnbins 0.5 --simplify-denovo 0 --simplify-annotated 0 --simplify-ir 0 --simplify -1`

Next the resultant adrenal gland sj (splice junction) files where built as an experimental group using a gff3 annotation for *CYP11B1* (file `CYP11B1_ensembl.Homo_sapiens.GRCh38.94.gff3` provided) and the settings file (`settings_build.ini`) with the following command:

`build CYP11B1_ensembl.Homo_sapiens.GRCh38.94.gff3 -j 4 --incremental -c settings_build.ini -o ./build --disable-denovo --min-intronic-cov 10`

### MAJIQ PSI 
MAJIQ PSI quantification files were generated with the following command:

```for i in `cat sample_list.txt`; do majiq psi -j 1 ./build/$i.majiq -n $i -o psi/; done```

### Voila view TSV 
Finally tab separated value files for the MAJIQ quantifications were generated using voila with the following command:

```for i in `cat sample_list.txt`; do voila tsv -f  psi_tsvs/$i.psi.tsv --show-all psi/$i.psi.voila build/splicegraph.sql; done```

## MAJIQtl 
sQTL mapping was performed on the PSI tsv files generated above using [MAJIQtl](https://bitbucket.org/biociphers/majiq_sqtl/src/master/) with the provided settings file (`settings_sqtl.ini`); the VCF file downloaded from dbGaP from the GTEx Consortium; and the covariate file provided by GTEx for adrenal gland based on splicing quantifications (`Adrenal_Gland.v8.sqtl_covariates.txt` from the tar.gz file download from the [GTEx portal](https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_sQTL_covariates.tar.gz). The parameters were specified using the following command:

`python call_sqtls.py settings_sqtl.ini sqtl_output.txt -v GTEx_Analysis_2017-06-05_v8_WholeGenomeSeq_838Indiv_Analysis_Freeze.SHAPEIT2_phased.vcf.gz -c Adrenal_Gland.v8.sqtl_covariates.txt -s linreg --normalize quantile -r 10000 --extend-exons --show-all`

Sex stratefied analysis was performed exactly as above except `settings_sqtl_male.ini` and `settings_sqtl_female.ini` were passed to `call_sqtls.py` instead. 

References 
-----
* Grgic, O., Gazzara, M.R., Chesi, A., Medina-Gomez, C., …, Barash, Y., Grant, S.F.A., Rivadeneira, F. CYP11B1 variants influence skeletal maturation via alternative splicing. (Accepted: Nature Comm. Bio.) 
* GTEx Consortium. (2020). The GTEx Consortium atlas of genetic regulatory effects across human tissues. Science, 369(6509), 1318-1330.
* Vaquero-Garcia, J., Barrera, A., Gazzara, M. R., Gonzalez-Vallinas, J., Lahens, N. F., Hogenesch, J. B., ... & Barash, Y. (2016). A new view of transcriptome complexity and regulation through the lens of local splicing variations. elife, 5, e11752.
* Nurnberg, S. T., Guerraty, M. A., Wirka, R. C., Rao, H. S., Pjanic, M., Norton, S., ... & Rader, D. J. (2020). Genomic profiling of human vascular cells identifies TWIST1 as a causal gene for common vascular diseases. PLoS genetics, 16(1), e1008538.
* Aicher, J. K., Jewell, P., Vaquero-Garcia, J., Barash, Y., & Bhoj, E. J. (2020). Mapping RNA splicing variations in clinically accessible and nonaccessible tissues to facilitate Mendelian disease diagnosis using RNA-seq. Genetics in Medicine, 22(7), 1181-1190.

